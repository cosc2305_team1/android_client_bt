/**
 * file: MainActivity.java
 * author: Team 1
 
 * Source code for Sensor functionality copied and modified from:
 * https://github.com/google-developer-training/android-advanced/tree/master/TiltSpot
 * Copyright 2017 Google, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package final_project.client2;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorMagnetometer;

    private float[] mAccelerometerData = new float[3];
    private float[] mMagnetometerData = new float[3];

    private TextView mTextSensorAzimuth;
    private TextView mTextSensorPitch;
    private TextView mTextSensorRoll;

    private static final float ROTATION_VALUE_DRIFT = 0.05f;

    private static float azimuth = 0;
    private static float pitch = 0;
    private static float roll = 0;

    Button connectBtn = null;

    private static boolean BLUETOOTH_RPI_CONNECTED = false;
    private static boolean SEND_SENSOR_DATA_ENABLED = false;
    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    BluetoothDevice remoteDevice = null;
    private OutputStream btSocketOutStream = null;

    private static final UUID MY_UUID =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static String MAC_ADDRESS_RPI = "B8:27:EB:9F:75:D1";

    private static String bufferSignal = "x";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // TODO - buttons global?
        connectBtn = (Button) findViewById(R.id.connectBtn);
        connectBtn.setOnClickListener(connectBtnListener);

        Button moveBtn = (Button) findViewById(R.id.moveBtn);
        moveBtn.setOnClickListener(moveBtnListener);

        Button stopBtn = (Button) findViewById(R.id.stopBtn);
        stopBtn.setOnClickListener(stopBtnListener);

        Button disconnectBtn = (Button) findViewById(R.id.disconnectBtn);
        disconnectBtn.setOnClickListener(disconnectBtnListener);

        mTextSensorAzimuth = (TextView) findViewById(R.id.value_azimuth);
        mTextSensorPitch = (TextView) findViewById(R.id.value_pitch);
        mTextSensorRoll = (TextView) findViewById(R.id.value_roll);

        mSensorManager = (SensorManager) getSystemService(
                Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER);
        mSensorMagnetometer = mSensorManager.getDefaultSensor(
                Sensor.TYPE_MAGNETIC_FIELD);

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        CheckBTState();

    }

    private void CheckBTState() {

        if(btAdapter==null) {
            // TODO display "Device does not support bluetooth."
        } else {
            if (btAdapter.isEnabled()) {
                // TODO display "bluetooth is enabled."
            } else {
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        if (mSensorAccelerometer != null) {
            mSensorManager.registerListener((SensorEventListener) this, mSensorAccelerometer,
                    1000000);
        }
        if (mSensorMagnetometer != null) {
            mSensorManager.registerListener((SensorEventListener) this, mSensorMagnetometer,
                    1000000);
        }
        //SensorManager.SENSOR_DELAY_NORMAL
    }

    @Override
    protected void onStop() {
        super.onStop();

        mSensorManager.unregisterListener((SensorEventListener) this);
}

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        int sensorType = sensorEvent.sensor.getType();

        // ADDED 11/29/18
        // Flushes event queue to mitigate queue delay.
        mSensorManager.flush(this);

        switch (sensorType) {
            case Sensor.TYPE_ACCELEROMETER:
                mAccelerometerData = sensorEvent.values.clone();
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mMagnetometerData = sensorEvent.values.clone();
                break;
            default:
                return;
        }

        float[] rotationMatrix = new float[9];
        boolean rotationOK = SensorManager.getRotationMatrix(rotationMatrix,
                null, mAccelerometerData, mMagnetometerData);

        float orientationValues[] = new float[3];
        if (rotationOK) {
            SensorManager.getOrientation(rotationMatrix, orientationValues);
        }

        azimuth = orientationValues[0];
        pitch = orientationValues[1];
        roll = orientationValues[2];

        mTextSensorAzimuth.setText(getResources().getString(
                R.string.value_format, azimuth));
        mTextSensorPitch.setText(getResources().getString(
                R.string.value_format, pitch));
        mTextSensorRoll.setText(getResources().getString(
                R.string.value_format, roll));

        if (SEND_SENSOR_DATA_ENABLED){
            sendRotationValuesToRPI(azimuth, pitch, roll);
        }

    } // END onSensorChanged()

    private void sendRotationValuesToRPI(float azimuth, float pitch, float roll) {

        //try {
        //    TimeUnit.SECONDS.sleep(1);
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        //}

        try {
            btSocketOutStream = btSocket.getOutputStream();
        } catch (IOException e) {
            //TODO - catch exception
        }

        String sAzimuth = String.valueOf(azimuth);
        String sPitch = String.valueOf(pitch);
        String sRoll = String.valueOf(roll);
        String outString = sAzimuth + bufferSignal + sPitch + bufferSignal + sRoll + bufferSignal;

        byte[] outByte = outString.getBytes();

        try {
            btSocketOutStream.write(outByte);
        } catch (IOException e) {
            // TODO - catch exception
        }

       /*
        if (btSocketOutStream != null) {
            try {
                btSocketOutStream.close();
            } catch (IOException e) {
                //TODO - catch exception
            }
        }
        */
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    private OnClickListener connectBtnListener = new OnClickListener() {
        public void onClick(View v) {

            if(!btAdapter.isEnabled()){
                // TODO - EXIT
            }

            remoteDevice = btAdapter.getRemoteDevice(MAC_ADDRESS_RPI);

            if (remoteDevice != null){

                try {
                    connectBtn.setText("Connecting...");
                    btSocket = remoteDevice.createRfcommSocketToServiceRecord(MY_UUID);
                    btSocket.connect(); // CAUTION - THIS FUNCTION MAY BLOCK APP !!
                    BLUETOOTH_RPI_CONNECTED = true;
                    connectBtn.setText("Connected");
                } catch (IOException e) {
                    // TODO - catch exception
                    BLUETOOTH_RPI_CONNECTED = false;
                    connectBtn.setText("Connect");
                }

            } else{
                // TODO - display "Cannot get remoteDevice from btAdapter."
                BLUETOOTH_RPI_CONNECTED = false;
            }
        }
    };

    private OnClickListener moveBtnListener = new OnClickListener() {
        public void onClick(View v) {
            if (BLUETOOTH_RPI_CONNECTED){
                SEND_SENSOR_DATA_ENABLED = true;
            } else{
                SEND_SENSOR_DATA_ENABLED = false;
            }

        }
    };

    private OnClickListener stopBtnListener = new OnClickListener() {
        public void onClick(View v) {
            SEND_SENSOR_DATA_ENABLED = false;
        }
    };

    private OnClickListener disconnectBtnListener = new OnClickListener() {
        public void onClick(View v) {
            SEND_SENSOR_DATA_ENABLED = false;

            if (BLUETOOTH_RPI_CONNECTED) {

                try {
                    btSocketOutStream = btSocket.getOutputStream();
                } catch (IOException e) {
                    //TODO - catch exception
                }

                // TODO - why is "d" being received with other values?
                // Server will receive *134132 etc...
                String out = "d";
                byte[] disconnectSignal = out.getBytes();

                try {
                    btSocketOutStream.write(disconnectSignal,0,1);
                } catch (IOException e) {
                    // TODO - catch exception
                }

            }

            if (btSocketOutStream != null) {
                try {
                    btSocketOutStream.flush();
                } catch (IOException e) {
                    // TODO - handle exception
                }
            }
        }
    };




} // end MainActivity
